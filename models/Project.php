<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "project".
 *
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property double $sum
 * @property string $date_begin
 * @property string $date_end
 */
class Project extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'project';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
                [['user_id', 'name', 'sum', 'date_begin', 'date_end'], 'required'],
                [['sum'], 'number'],
                [['name'], 'string', 'max' => 255],
                [['date_begin', 'date_end'], 'filter', 'filter' => function($value) {
                    if ($value == '')
                        return NULL;
                    return date('Y-m-d', strtotime($value));
                }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'userName' => 'Пользователь',
            'name' => 'Название',
            'sum' => 'Стоимость',
            'date_begin' => 'Дата начала',
            'date_end' => 'Дата сдачи',
        ];
    }

    // user 	
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getUserName() {
        if (isset($this->user))
            return $this->user->username;
        else
            return '';
    }

}
