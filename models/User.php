<?php

namespace app\models;

use yii\web\IdentityInterface;
use Yii;

class User extends \yii\db\ActiveRecord implements IdentityInterface {

    public static function tableName() {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
                [['username', 'password'], 'required'],
                [['username', 'password'], 'string', 'max' => 20],
                [['username'], function ($attribute, $params) {
                    if (!preg_match('/^[a-zA-Z0-9_]+$/', $this->$attribute))
                        $this->addError($attribute, 'Можно вводить только латинские буквы, цифры и нижний прочерк.');
                }],
                [['username'], function ($attribute, $params) {
                    $users = Yii::$app->db->createCommand('SELECT id FROM user WHERE username = ' . "'" . $this->$attribute . "'")
                            ->queryAll();
                    if (!count($users) == 0)
                        $this->addError($attribute, 'Пользователь с таким именем уже существует.');
                }],
                [['password'], function ($attribute, $params) {
                    if (!preg_match('/^[a-zA-Z0-9]+$/', $this->$attribute))
                        $this->addError($attribute, 'Можно вводить только латинские буквы и цифры.');
                }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'username' => 'Имя пользователя',
            'log' => 'log',
            'password' => 'Пароль',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id) {
        return static::findOne($id);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null) {
        /* foreach (self::$users as $user) {
          if ($user['accessToken'] === $token) {
          return new static($user);
          }
          } */

        return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username) {
        return static::findOne(['username' => $username]);
    }

    /**
     * {@inheritdoc}
     */
    public function getId() {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey() {
        //return $this->authKey;
        return '';
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey) {
        //return $this->authKey === $authKey;
        return true;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password) {
        //return \Yii::$app->security->validatePassword($password, $this->password);
        return $this->password === $password;
    }

}
