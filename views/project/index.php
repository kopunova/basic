<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProjectSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Проекты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <div class="panel panel-default panel-body">

        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                'id',
                'userName',
                'name',
                'sum',
                    [
                    'attribute' => 'date_begin',
                    'content' => function($data) {
                        return date('d.m.Y', strtotime($data->date_begin));
                    }
                ],
                    [
                    'attribute' => 'date_end',
                    'content' => function($data) {
                        return date('d.m.Y', strtotime($data->date_end));
                    }
                ],
                    ['class' => 'yii\grid\ActionColumn'],
            ],
        ]);
        ?>
    </div>
</div>












