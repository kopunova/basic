<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\Project */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="project-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="panel panel-default panel-body">

        <div class="row">

            <div class="col-md-8"> 	

                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

            </div>
            <div class="col-md-2">

                <?=
                $form->field($model, 'date_begin')->widget(\yii\jui\DatePicker::classname(), [
                    'language' => 'ru',
                    'dateFormat' => 'dd.MM.yyyy',
                    'options' => ['class' => 'form-control'],
                ])
                ?>	
            </div>
            <div class="col-md-2">

                <?=
                $form->field($model, 'date_end')->widget(\yii\jui\DatePicker::classname(), [
                    'language' => 'ru',
                    'dateFormat' => 'dd.MM.yyyy',
                    'options' => ['class' => 'form-control'],
                ])
                ?>

            </div>
        </div>

        <div class="row">
            <div class="col-md-8">

                <?php $users = User::find()->orderBy('username')->all(); ?>

                <?= $form->field($model, 'user_id')->dropDownList(ArrayHelper::Map($users, 'id', 'username'), ['prompt' => 'Выберите пользователя...']); ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'sum')->textInput() ?>
            </div>
        </div>


    </div>
    <div class="form-group">
        <?= Html::submitButton('Записать', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>	

</div>











