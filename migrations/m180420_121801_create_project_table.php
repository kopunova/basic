<?php

use yii\db\Migration;

/**
 * Handles the creation of table `project`.
 */
class m180420_121801_create_project_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('project', [
            'id' => $this->primaryKey(),
			'user_id' => $this->integer(11)->notNull(),
			'name' => $this->string(),
			'sum' => $this->float(),
			'date_begin' => $this->date(),
			'date_end' => $this->date(),
        ]);		
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('project');
    }
}















