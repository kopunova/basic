<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m180420_121719_create_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
			'username' => $this->string()->notNull()->unique(),
			'log' => $this->string(),
			'password' => $this->string(),
        ]);
		
		$this->insert('user', [
			'username' => 'admin',
			'log' => 'admin',
			'password' => 'admin',            
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('user');
    }
}
















